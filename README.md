# Signal Proxy

<a href="https://libreops.cc"><img src="https://libreops.cc/static/img/libreops.svg" width="300"></a>

Ansible scripts for setting up LibreOps [Signal Proxy](https://libreops.cc/signalproxy.html).

## Requirements

1. Ansible
2. GPG
3. Running gpg-agent

## Usage

1. Install LibreOps Ansible collection:

```bash
ansible-galaxy collection install git+https://gitlab.com/libreops/galaxy
```

2. Decrypt the `hosts` file:

```bash
gpg -d hosts.gpg > hosts
```

3. Verify:

```bash
ansible all -v -m ping -i hosts
```

### Playbooks

Run the whole thing:

```bash
ansible-playbook -v main.yml
```

## Vault

You should be able to use vault with your gpg key:

```bash
ansible-vault view vault/main.yml
```

## Support

[![OpenCollective](https://img.shields.io/opencollective/all/libreops?color=%23800&label=opencollective&style=flat-square)](https://opencollective.com/libreops/)

## Join

[![irc](https://img.shields.io/badge/Matrix-%23libreops:matrix.org-blue.svg)](https://riot.im/app/#/room/#libreops:matrix.org)

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
